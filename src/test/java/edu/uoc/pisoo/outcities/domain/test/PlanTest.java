package edu.uoc.pisoo.outcities.domain.test;

import edu.uoc.pisoo.outcities.domain.Plan;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class PlanTest {

    @Test
    public void testVisitasPlan(){
        Plan plan = new Plan(12L);
        plan.nuevaVisita();
        plan.nuevaVisita();
        plan.nuevaVisita();
        assertEquals(3,plan.getNumVisitas());
    }

    @Test
    public void testDatosPlan(){
        Plan plan = new Plan(13L);
        assertEquals(new Long(13),plan.getId());
        assertEquals(0,plan.getNumVisitas());
        assertEquals(0,plan.getOpiniones().size());
        assertEquals(0,plan.getFans().size());
    }
}
