package edu.uoc.pisoo.outcities.domain;

import org.joda.time.DateTime;
import org.joda.time.Period;

import java.util.LinkedHashSet;
import java.util.Set;

public class Usuario {

    private final Long id;
    private String nombre;
    private String contrasena;
    private DateTime fechaNacimiento;
    private EmailAddress email;
    private final LinkedHashSet<Plan> favoritos = new LinkedHashSet<Plan>();
    private final LinkedHashSet<Opinion> opiniones = new LinkedHashSet<Opinion>();
    private final LinkedHashSet<Zona> zonas = new LinkedHashSet<Zona>();

    /** Atributo derivado edad */
    public int getEdad(){
        return new Period(fechaNacimiento,new DateTime()).getYears();
    }

    public Usuario(Long id, String nombre, String contrasena, DateTime fechaNacimiento, EmailAddress email) {
        this.id = id;
        this.nombre = nombre;
        this.contrasena = contrasena;
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
    }

    public void cambiarDatos(String nombre, DateTime fechaNacimiento, EmailAddress email){
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
    }

    public void cambiarContrasena(String contrasena, String verificacionContrasena) {
        if(contrasena==null || !contrasena.equals(verificacionContrasena)) throw new IllegalArgumentException();
        this.contrasena = contrasena;
    }

    public void opinar(Plan plan, TipoOpinion tipo, String comentario){
        for(Opinion opinion : opiniones){
            if(opinion.getPlan().equals(plan)){
                opinion.modificar(tipo, comentario);
                return;
            }
        }
        opiniones.add(new Opinion(tipo,comentario,plan));
    }

    public boolean checkContrasena(String contrasena){
        return this.contrasena.equals(contrasena);
    }

    public void borrarOpinion(Plan plan) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void anadirFavorito(Plan plan) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void quitarFavorito(Plan plan) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    // Getters generados

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public DateTime getFechaNacimiento() {
        return fechaNacimiento;
    }

    public EmailAddress getEmail() {
        return email;
    }

    public Set<Plan> getFavoritos() {
        return favoritos;
    }

    public LinkedHashSet<Opinion> getOpiniones() {
        return opiniones;
    }

    public Set<Zona> getZonas() {
        return zonas;
    }

}
