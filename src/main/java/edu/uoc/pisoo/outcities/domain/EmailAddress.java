package edu.uoc.pisoo.outcities.domain;

public class EmailAddress {
    private String email;

    public EmailAddress(String email) {
        if(email==null) throw new IllegalArgumentException();
        this.email = email;
    }

    @Override
    public String toString() {
        return email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmailAddress that = (EmailAddress) o;
        return email.equals(that.email);
    }

    @Override
    public int hashCode() {
        return email != null ? email.hashCode() : 0;
    }
}
